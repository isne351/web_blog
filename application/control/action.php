<?php
Class action extends action_control
{

	public function register()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}		

		if(input::post('password') == input::post('password_confirm')){

			$this->instance->user->username = input::post('username');
			$this->instance->user->password = input::post('password');

			$this->instance->user->name = input::post('name');
			$this->instance->user->gender = input::post('gender');

			if($this->instance->user->register() == true){
				alert(base('login'),'Register finish!');
			}else{
				result($this->instance->user->get_error());
			}

		}else{
				result('Password != Comfirm Password');			
		}
		
	}

	public function login()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}

		$this->instance->user->username = input::post('username');
		$this->instance->user->password = input::post('password');

		if($this->instance->user->login() == true){
			$_SESSION['login'] = $this->instance->user->user_id;
			redirect(base('dashboard'));
		}else{
			result($this->instance->user->get_error());
		}

	}

	public function logout(){
		session_destroy();
		redirect(base('dashboard'));
	}

	public function post($action){

		$this->instance->loader->app->helper('upload');
		$this->instance->loader->app->model('post');

		switch ($action) {
			case 'insert':

					$image = empty($_FILES['cover']) ? '' : $_FILES['cover'];

			        if (!empty($image))
			        {
			            if ($image['size'] > 0)
			            {
			            	$this->instance->post->cover = upload($image);
			            }
			        }

					$this->instance->post->topic = input::post('topic');
					$this->instance->post->category = input::post('category');
					$this->instance->post->content = input::post('content');

					$this->instance->post->comment_setting = input::post('comment');
					$this->instance->post->privacy = input::post('privacy');

					$this->instance->post->owner = $this->instance->user->user_id;

					if($this->instance->post->insert() == true){
						alert(base('post/'.$this->instance->post->post_id),'Post success');	
					}else{
						result($this->instance->post->get_error());
					}

				break;
			
			case 'update':

					$image = empty($_FILES['cover']) ? '' : $_FILES['cover'];


			        if (!empty($image)){
			            if ($image['size'] > 0){
			            	$this->instance->post->cover = upload($image);
			            }else{
			        		$this->instance->post->cover = input::post('cover');
			        	}
			        }else{
			        	$this->instance->post->cover = input::post('cover');
			        }

					$this->instance->post->topic = input::post('topic');
					$this->instance->post->category = input::post('category');
					$this->instance->post->content = input::post('content');

					$this->instance->post->comment_setting = input::post('comment');
					$this->instance->post->privacy = input::post('privacy');

					$this->instance->post->post_id = input::post('post_id');
					$this->instance->post->owner = $this->instance->user->user_id;
					
					if($this->instance->post->update() == true){
						result('Edit success!');
					}else{
						result($this->instance->post->get_error());
					}

				break;

			case 'delete':

					$this->instance->post->post_id = input::get('post_id');
					$this->instance->post->owner = $this->instance->user->user_id;
					
					if($this->instance->post->delete() == true){
						alert(base(),'Delete success!');
					}else{
						result($this->instance->post->get_error());
					}

				break; 	
		}

	}

	public function comment($action){


		$this->instance->loader->app->model('comment');

		switch ($action) {
			case 'insert':

					$this->instance->comment->content = input::post('content');
					$this->instance->comment->post_id = input::post('post_id');
					$this->instance->comment->owner = $this->instance->user->user_id;

					if($this->instance->comment->insert() == true){
						result('Comment success!');
					}else{
						result($this->instance->comment->get_error());
					}

				break;
			case 'delete':
					$this->instance->comment->comment_id = input::get('comment_id');
					$this->instance->comment->owner = $this->instance->user->user_id;

					if($this->instance->comment->delete() == true){
						result('Comment success!');
					}else{
						result($this->instance->comment->get_error());
					}

				break;
		}

	}

	public function uploadimage(){

		$this->instance->loader->app->helper('upload');

		$image = empty($_FILES['image']) ? '' : $_FILES['image'];

        if (!empty($image))
        {
            if ($image['size'] > 0)
            {
            	echo upload($image);
            }
        }

	}

}