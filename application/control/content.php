<?php
Class content extends page_control{

	public function __construct(){
		parent::__construct();

		$this->instance->loader->app->helper('post_block');

		$this->instance->loader->app->library('feed');		
	}

	public function category($category_id)
	{
		$this->instance->template->set_view('body','category');

		$post = $this->instance->feed->get_post_by_category($category_id,20);		

		$this->instance->template->set_var('post',$post);
	}	

	public function user($user_id)
	{

		$this->instance->template->set_view('body','user');

		$post = $this->instance->feed->get_post_by_user($user_id,20);		

		$this->instance->template->set_var('post',$post);

	}

	public function search()
	{
		$this->instance->template->set_view('body','search');

		$post = $this->instance->feed->get_post_by_search(input::post('keyword'),20);		

		$this->instance->template->set_var('post',$post);
		$this->instance->template->set_var('keyword',input::post('keyword'));
	}

	public function post($post_id)
	{
		$this->instance->template->set_view('body','content/post');
		
		$post = $this->instance->feed->get_post_by_id($post_id);		
		
		if($post->post_id == false){
			redirect(base('404'));
		}

		$this->instance->template->set_var('post',$post);
	}

}