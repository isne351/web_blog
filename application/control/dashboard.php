<?php
Class dashboard extends page_control{

	public function index()
	{

		$this->instance->loader->app->helper('post_block');

		$this->instance->loader->app->model('category');
		$this->instance->loader->app->library('feed');

		$this->instance->template->set_view('body','dashboard');

		$post = $this->instance->feed->get_post_all(10);		
		$category = $this->instance->category->get_all();

		$this->instance->template->set_var('category',$category);
		$this->instance->template->set_var('post',$post);

	}

	public function all_post()
	{
		$this->instance->loader->app->helper('post_block');

		$this->instance->loader->app->library('feed');

		$this->instance->template->set_view('body','post_all');

		$post = $this->instance->feed->get_post_all(30);		

		$this->instance->template->set_var('post',$post);
	}

	public function about(){
		$this->instance->template->set_view('body','dashboard');
	}


}