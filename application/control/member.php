<?php
Class member extends page_control{

	public function __construct(){
		parent::__construct();

		$this->instance->template->set_view('navbar',null);

	}

	public function login()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}

		$this->instance->template->set_view('body','member/login');

	}

	public function register()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}

		$this->instance->template->set_view('body','member/register');

	}

	public function post($action,$post_id = 0){

		if($this->instance->user->is_login() == false){
			redirect(base('dashboard'));
		}

		$this->instance->loader->app->helper('post_block');
		$this->instance->loader->app->library('feed');		

		$this->instance->template->set_view('navbar','navbar/user');

		switch ($action) {
			case 'insert':
				$this->instance->template->set_view('body','member/post');
				break;
			case 'update':

				$post = $this->instance->feed->get_post_by_id($post_id);		

				if(($post->owner->user_id == $this->instance->user->user_id) or ($this->instance->user->level == 'admin')){
					$this->instance->template->set_var('post',$post);			
					$this->instance->template->set_view('body','member/post_update');
				}else{
					redirect(base('404'));
				}
				break;			
		}

		

	}


}