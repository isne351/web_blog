<?php

function index_post(post_model $post)
{
    $html='             <h2 style="overflow:hidden; text-overflow: ellipsis;    white-space: nowrap;">';
    $html.='                <a href="'.base('post/'.$post->post_id).'">'.$post->topic.'</a>';
    $html.='            </h2>';
    $html.='            <p class="lead">';
    $html.='                by <a href="'.base('post/by/'.$post->owner->user_id).'">'.$post->owner->name.'</a>';
    $html.='            </p>';
    if($post->cover == true){
        $html.='            <div class="cover"><img src="'.$post->cover.'" alt=""></div>';
    }
    $html.='            <hr>';
    $html.='            <p>'.preg_replace ('/<[^>]*>/',' ',$post->content).'</p>';
    $html.='            <a class="btn btn-primary" href="'.base('post/'.$post->post_id).'">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>';
    $html.='            <hr>';
    $html.='            <p><span class="glyphicon glyphicon-time"></span> Posted on '.$post->create_date.'</p>';
    $html.='            <hr>';	

	return $html;
}

function comment(comment_model $comment,$delete = false)
{
    $html='    <h3 class="lead">';
    $html.='      <a href="'.base('post/by/'.$comment->owner->user_id).'">'.$comment->owner->name.'</a> say';
    if($delete == true){
        $html.='      <a href="'.base('action/comment/delete?comment_id='.$comment->comment_id).'" style="color:red;font-size:12px;"> delete </a>';
    }
    $html.='      <p>'.$comment->content.'</p>';
    $html.='   </h3>';
    $html.='   <p><span class="glyphicon glyphicon-time"></span> comment on '.$comment->create_date.'</p>';

    return $html;
}

function comment_form(post_model $post,user_model $user)
{

    $html='<form action="'.base('action/comment/insert').'" method="POST">';
    $html.='<h2>'.$user->name.' <small> say </small></h2>';
    $html.='<input type="hidden" name="post_id" value="'.$post->post_id.'">';
    $html.='<input class="form-control" type="text" name="content" placeholder="Type you comment here!"></input>';
    $html.='</form>';

    return $html;
}

function category_option($active = false)
{

    $instance = get_instance();

    $instance->loader->app->model('category');

    $category = $instance->category->get_all();

    $html  = '<select name="category" class="form-control">';
    
    foreach ($category as $key => $value) {
      if($value->category_id == $active){
            $html .= '<option value="'.$value->category_id.'" selected>'.$value->category_name.'</option>';
      }else{
            $html .= '<option value="'.$value->category_id.'">'.$value->category_name.'</option>';
      }
    }

    $html .= '</select>';

    return $html;

}