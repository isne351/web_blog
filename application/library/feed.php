<?php 

class feed extends library{
	
	public function __construct(){
		parent::__construct();

		$this->instance->loader->database->connect();
        $this->instance->loader->app->model('post');
        $this->instance->loader->app->model('category');
        $this->instance->loader->app->model('comment');
        $this->instance->loader->app->model('user');

	}

	protected function get_comment($post_id){

		$comment_array = $this->instance->comment->get_by_postid($post_id);

		$comment_list = array();

		foreach($comment_array as $comment){
			$user = $this->instance->user->get_by_id($comment->owner);
			$comment->owner = $user;
			$comment_list[] = $comment;
		}

		return array('amount'=> '','list' => $comment_list);

	}

	public function get_post_by_id($id)
	{
		$post = $this->instance->post->get_by_id($id);

		$user = $this->instance->user->get_by_id($post->owner);

		$category = $this->instance->category->get_by_id($post->category);

		$comment = $this->get_comment($id);

		$post->owner = $user;
		$post->comment = $comment['list'];
		$post->categry = $category;

		return $post;
	}

	public function get_post_by_search($keyword,$limit = false, $offset = false)
	{
		$post_array = $this->instance->post->get_by_search($keyword,$limit,$offset);
		
		$post_list = array();

		foreach ($post_array as $key => $post) {
			
			$comment = $this->get_comment($post->post_id);

			$category = $this->instance->category->get_by_id($post->category);

			$user = $this->instance->user->get_by_id($post->owner);

			$post->owner = $user;
			$post->category = $category;

			$post_list[] = $post;

		}

		return array('amount'=> '','list' => $post_list);		
	}

	public function get_post_by_user($owner,$limit = false, $offset = false)
	{

		$post_array = $this->instance->post->get_by_owner($owner,$limit,$offset);

		$user = $this->instance->user->get_by_id($owner);

		$post_list = array();

		foreach ($post_array as $key => $post) {
			
			$comment = $this->get_comment($post->post_id);

			$category = $this->instance->category->get_by_id($post->category);

			$post->owner = $user;
			$post->category = $category;

			$post_list[] = $post;

		}

		return array('amount'=> '','list' => $post_list,'owner'=>$user);

	}

	public function get_post_by_category($category_id ,$limit = false, $offset = false)
	{

		$category = $this->instance->category->get_by_id($category_id,$limit,$offset);

		$post_array = $this->instance->post->get_by_category($category_id);

		$post_list = array();

		foreach ($post_array as $key => $post) {
			
			$comment = $this->get_comment($post->post_id);

			$user = $this->instance->user->get_by_id($post->owner);		

			$post->owner = $user;
			$post->category = $category;

			$post_list[] = $post;

		}

		return array('amount'=> '','list' => $post_list,'category'=>$category);

	}

	public function get_post_all($limit = false ,$offset = false)
	{

		$post_array = $this->instance->post->get_all($limit,$offset);

		$post_list = array();

		foreach ($post_array as $key => $post) {

			$user = $this->instance->user->get_by_id($post->owner);

			$category = $this->instance->category->get_by_id($post->category);

			$comment = $this->get_comment($post->post_id);

			$post->owner = $user;
			$post->category = $category;

			$post_list[] = $post;

		}

		return array('amount'=> '','list' => $post_list);

	}

}

?>