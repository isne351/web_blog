<?php
Class post_model extends model{

    protected $object = array(
    		'post_id'=>'',
    		'topic'=>'',
    		'content'=>'',
    		'category'=>'',
            'cover'=>'',
            'comment_setting'=>'close',
            'privacy'=>'private',
    		'owner'=>'',
    		'create_date'=>'',
            'comment'=>array()
    		);
    
    protected function comment_setting($comment_setting){
        if($comment_setting == false){
            return 'private';
        }

        $allow = array('public','user','close');

        if(in_array($comment_setting,$allow) == false){
            throw new Exception('invalid comment_setting!');
        }

        return $comment_setting;
    }    

    protected function privacy($privacy){

        if($privacy == false){
            return 'private';
        }

        $allow = array('public','private');

        if(in_array($privacy,$allow) == false){
            throw new Exception('invalid comment_setting!');
        }

        return $privacy;
    } 

    public function get_by_search($keyword,$limit = 40,$offset = false)
    {
        $this->instance->query->order('DESC','`post_info`.`create_date`'); 
        $this->instance->query->limit($limit,$offset);
        $this->instance->query->where('`topic` LIKE :topic');
        $this->instance->query->or_where('`content` LIKE :content');

        $query = $this->instance->query->exec('select','post_info');


        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':topic',"%".$keyword."%",PDO::PARAM_STR);
        $exec->bindValue(':content',"%".$keyword."%",PDO::PARAM_STR);

        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
            $list[] = new post_model($value);
        }

        return $list;
    }

    public function get_all($limit = 40,$offset = false){

        if($this->instance->user->is_login() == false){
            $this->instance->query->where("`privacy` = 'public'");
        }

        $this->instance->query->order('DESC','`post_info`.`create_date`'); 
        $this->instance->query->limit($limit,$offset);
        $query = $this->instance->query->exec('select','post_info');

        $exec = $this->instance->database->prepare($query);

        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
            $list[] = new post_model($value);
        }

        return $list;

    }

    public function get_by_owner($owner,$limit = 40,$offset = false){

        if($this->instance->user->is_login() == false){
            $this->instance->query->where("`privacy` = 'public'");
        }

        $this->instance->query->order('DESC','`post_info`.`create_date`'); 
        $this->instance->query->limit($limit,$offset);
        $this->instance->query->where('owner = :owner');

        $query = $this->instance->query->exec('select','post_info');

        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':owner',$owner);
        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
            $list[] = new post_model($value);
        }

        return $list;

    }

    public function get_by_category($category,$limit = 40,$offset = false){

        if($this->instance->user->is_login() == false){
            $this->instance->query->where("`privacy` = 'public'");
        }

        $this->instance->query->order('DESC','`post_info`.`create_date`'); 
        $this->instance->query->limit($limit,$offset);
        $this->instance->query->where('category = :category');

        $query = $this->instance->query->exec('select','post_info');

        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':category',$category);
        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
            $list[] = new post_model($value);
        }

        return $list;

    }

    public function get_by_id($post_id){

        if($this->instance->user->is_login() == false){
            $this->instance->query->where("`privacy` = 'public'");
        }

        $this->instance->query->where('`post_info`.`post_id` = :post_id ');

        $query = $this->instance->query->exec('select','post_info');
        
        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':post_id',$post_id);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        $post = new post_model($fetch);
    
        return $post;

    }

    public function insert(){

        $this->instance->query->set('topic');
        $this->instance->query->set('category');
        $this->instance->query->set('content');
        $this->instance->query->set('owner');

        $this->instance->query->set('cover');
        $this->instance->query->set('comment_setting');
        $this->instance->query->set('privacy');

        $query = $this->instance->query->exec('insert','post_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':topic',$this->topic,PDO::PARAM_STR);
            $exec->bindValue(':category',$this->category,PDO::PARAM_STR);
            $exec->bindValue(':content',$this->content,PDO::PARAM_STR);
            $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);
            $exec->bindValue(':cover',$this->cover,PDO::PARAM_STR);
            $exec->bindValue(':comment_setting',$this->comment_setting,PDO::PARAM_INT);
            $exec->bindValue(':privacy',$this->privacy,PDO::PARAM_INT);

        $result = $exec->execute();

        $this->post_id = $this->instance->database->lastInsertId();

        return $result;

    }

    public function update()
    {

        if(($this->instance->user->user_id == $this->owner) or ($this->instance->user->level == 'admin')){

            $this->instance->query->where('`post_id` = :post_id');

            $this->instance->query->set('topic');
            $this->instance->query->set('category');
            $this->instance->query->set('content');

            if($this->instance->user->level != 'admin'){
                $this->instance->query->where('`owner` = :owner');
            }
            
            $this->instance->query->set('cover');
            $this->instance->query->set('comment_setting');
            $this->instance->query->set('privacy');

            $this->instance->query->set('post_id');

            $query = $this->instance->query->exec('update','post_info');

            $exec = $this->instance->database->prepare($query);
                $exec->bindValue(':topic',$this->topic,PDO::PARAM_STR);
                $exec->bindValue(':category',$this->category,PDO::PARAM_STR);
                $exec->bindValue(':content',$this->content,PDO::PARAM_STR);

                $exec->bindValue(':cover',$this->cover,PDO::PARAM_STR);
                $exec->bindValue(':comment_setting',$this->comment_setting,PDO::PARAM_INT);
                $exec->bindValue(':privacy',$this->privacy,PDO::PARAM_INT);

                if($this->instance->user->level != 'admin'){
                    $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);
                }
                $exec->bindValue(':post_id',$this->post_id,PDO::PARAM_INT);

            $result = $exec->execute();   

            return $result;

        }else{
            return false;
        }
    }

    public function delete(){

        if(($this->instance->user->user_id == $this->owner) or ($this->instance->user->level == 'admin')){

            $this->instance->query->where('`post_id` = :post_id');
            
            if($this->instance->user->level != 'admin'){
                $this->instance->query->where('`owner` = :owner');
            }

            $query = $this->instance->query->exec('delete','post_info');

            $exec = $this->instance->database->prepare($query);
                $exec->bindValue(':post_id',$this->post_id,PDO::PARAM_INT);
                if($this->instance->user->level != 'admin'){
                    $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);
                }

            $result = $exec->execute();   

            return $result;

        }

    }

}
?>