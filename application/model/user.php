<?php
Class user_model extends model{

     protected $object =  array(
                            'user_id'=>'',
                            'username'=>'',
                            'password'=>'',
                            'level'=>'user',
                            'name'=>'',
                            'gender'=>'',
                        );
    private $login = false;

    protected function password($password){
        return sha1($this->username.sha1($password));
    }

    protected function username($username){
        if(preg_match('/^[A-z0-9_]{5,12}$/',$username) == false){
            throw new Exception('invalid username!');
        } 
        return $username;
    }

    protected function level($level){
        $allow = array('user','admin');

        if(in_array($level,$allow) == false){
            throw new Exception('invalid level!');
        }

        return $level;
    }

    public function is_login(){
        return $this->login;
    }

    public function get_by_id($id){

        $this->instance->query->where('`user_authen`.`user_id` = :user_id');
        $this->instance->query->join('left','user_profile','`user_profile`.`user_id` = `user_authen`.`user_id`');

        $query = $this->instance->query->exec('select','user_authen');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':user_id',$id,PDO::PARAM_INT);

        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        return new user_model($fetch);

    }

    public function check_login()
    {
        $this->instance->query->where('`user_authen`.`user_id` = :user_id');
        $this->instance->query->join('left','user_profile','`user_profile`.`user_id` = `user_authen`.`user_id`');

        $query = $this->instance->query->exec('select','user_authen');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':user_id',$this->user_id,PDO::PARAM_INT);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        if($fetch->user_id == $this->user_id){
            $this->fill($fetch);
            $this->login = true;
        }
    }

    public function login(){

        $this->instance->query->where('username = :username');
        $this->instance->query->where('password = :password');

        $query = $this->instance->query->exec('select','user_authen');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':username',$this->username,PDO::PARAM_STR);
            $exec->bindValue(':password',$this->password,PDO::PARAM_STR);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        $this->fill($fetch);

        if($fetch->username != $this->username){
             $this->error = 'login fail';
            return false;           
        }
        $this->login = true;

        return true;
    }

    public function register(){


        $this->instance->query->where('username = :username');

        $query = $this->instance->query->exec('select','user_authen');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':username',$this->username,PDO::PARAM_STR);
        $result = $exec->execute();
        
        if($exec->rowCount() > 0){
            $this->error = 'duplicate username!';
            return false;
        }    

        $this->instance->query->set('username');
        $this->instance->query->set('password');
        $this->instance->query->set('level');

        $query = $this->instance->query->exec('insert','user_authen');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':username',$this->username,PDO::PARAM_STR);
            $exec->bindValue(':password',$this->password,PDO::PARAM_STR);
            $exec->bindValue(':level',$this->level,PDO::PARAM_STR);

        $result = $exec->execute();

        $this->user_id = $this->instance->database->lastInsertId();

        $this->instance->query->set('name');
        $this->instance->query->set('gender');
        $this->instance->query->set('user_id');

        $query = $this->instance->query->exec('insert','user_profile');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':name',$this->name,PDO::PARAM_STR);
            $exec->bindValue(':gender',$this->gender,PDO::PARAM_STR);
            $exec->bindValue(':user_id',$this->user_id,PDO::PARAM_INT);
        $result = $exec->execute();

        return $result;

    }

}
?>