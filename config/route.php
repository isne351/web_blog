<?php
$config['route']['index'] = 'dashboard/index'; 
$config['route']['dashboard'] = 'dashboard/index'; 

$config['route']['post/all'] = 'dashboard/all_post'; 

$config['route']['category/([0-9]+)'] = 'content/category'; 
$config['route']['post/([0-9]+)'] = 'content/post'; 
$config['route']['post/by/([0-9]+)'] = 'content/user'; 
$config['route']['post/search'] = 'content/search'; 

$config['route']['login'] = 'member/login'; 
$config['route']['register'] = 'member/register'; 

$config['route']['member/post'] = 'member/post/insert'; 
$config['route']['profile'] = 'member/profile'; 

$config['route']['post/update/([0-9]+)'] = 'member/post/update'; 

$config['route']['action/login'] = 'action/login'; 
$config['route']['action/register'] = 'action/register'; 
$config['route']['action/logout'] = 'action/logout'; 

$config['route']['action/uploadimage'] = 'action/uploadimage';

$config['route']['action/post/insert'] = 'action/post/insert';
$config['route']['action/post/update'] = 'action/post/update';
$config['route']['action/post/delete'] = 'action/post/delete';

$config['route']['action/comment/insert'] = 'action/comment/insert';
$config['route']['action/comment/update'] = 'action/comment/update';
$config['route']['action/comment/delete'] = 'action/comment/delete';


$config['route']['404'] = 'error/page_not_found'; 