<?php
interface control{}

abstract class page_control implements control{

	protected $instance;

	public function __construct(){

		$this->instance = get_instance();

		$this->instance->loader->system->library('query');        
		$this->instance->loader->system->library('auth');

		$this->instance->template->set_view('header','layout/header');
		if($this->instance->user->is_login() == true){
			$this->instance->template->set_view('navbar','navbar/user');		
		}else{
			$this->instance->template->set_view('navbar','navbar/guest');
		}
		$this->instance->template->set_view('footer','layout/footer');

		$this->instance->template->set_var('site',get_config('site'));	
		$this->instance->template->set_var('user',$this->instance->user);
		if(isset($_SESSION['alert']) == true){

			echo '<script>alert("'.$_SESSION['alert'].'")</script>';
			unset($_SESSION['alert']);
		}

	}

}

abstract class json_control implements control{

	protected $instance;

	public function __construct(){
		$this->instance = get_instance();
		$this->instance->template->set_type('json');
		$this->instance->loader->system->library('query');    
		$this->instance->loader->system->library('auth');
	}

}

abstract class action_control implements control{

	protected $instance;

	public function __construct(){
		$this->instance = get_instance();
		$this->instance->template->set_type('page');

		$this->instance->loader->system->library('query');        
		$this->instance->loader->system->library('auth');

		$this->instance->template->error_type = 'alert';

	}

}