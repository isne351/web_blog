<?php
Class query extends library{
	
	private $build;
	public $sql;

	public function __construct(){
		parent::__construct();
		$this->build = new stdclass();
	}

	public function __call($function,$argument){

		switch($function){

			case 'group':
				$this->build->group = addslashes($argument[0]);
			break;

			case 'order':
				$this->build->order[] = array($argument[0]=>$argument[1]);
			break;

			case 'limit':
				$this->build->limit = $argument[0];
				if($argument[1] == true){
					$this->build->offset = $argument[1];
				}
			break;

			case 'select':
				$this->build->select[] = $argument[0]; 
			break;

			case 'join':
				$this->build->join[] = $argument; 
			break;

			case 'where':
				if(isset($argument[1]) == false){
					$group = 0;
				}else{
					$group = 1;
				}

				$this->build->where[$group][] = array('type'=>'and','value'=>$argument[0]);
			break;

			case 'or_where':
				if(isset($argument[1]) == false){
					$group = 0;
				}else{
					$group = 1;
				}

				$this->build->where[$group][] = array('type'=>'or','value'=>$argument[0]);
			break;

			case 'set':
				$this->build->set[] = $argument[0];
			break;

		}

	}

	Public function exec($function = 'select',$table){
	

		if(!isset($this->build->select)){
			$build['select']='*';
		}else{
			$build['select']=implode(',',$this->build->select);
		}

		if(!isset($this->build->group)){
			$build['group']='';
		}else{
			$build['group']='group by '.$this->build->group;
		}
		
		if(!isset($this->build->limit)){
			$build['limit']='';
		}else{
			if(!isset($this->build->offset)){
					$build['limit']=' limit '.$this->build->limit;			
			}else{
					$build['limit']=' limit '.$this->build->limit.','.$this->build->offset;			
			}
		}
		
		if(!isset($this->build->join)){
			$build['join']='';
		}else{

			$joins = '';

			foreach($this->build->join as $join){
				if(isset($join['type'])){
					$joins = $join['type'].' join '.$join['table'].' ON '.$join['condition'];
				}else{
					$joins .= $join[0].' join '.$join[1].' ON '.$join[2];
				}
			}
			
			$build['join']=$joins;
		}

		if(!isset($this->build->order)){		
			$build['order']='';
		}else{			
			$i=0;
			$orders=' order by ';
			foreach ($this->build->order as $key=>$order){
				$i++;
				$orders.=addslashes(end($order)).' '.addslashes(key($order));
				if(count($this->build->order)>$i){
					$orders.=',';
				}
			}
			$build['order']=$orders;
		}
		
		if(!isset($this->build->where)){
			$build['where']= 1 ;
		}else{

			$where='';

			foreach ($this->build->where as $group_key => $group_value) // loop group
			{
				$where.='(';

				$count = count($group_value);

				foreach ($group_value as $key=>$codition) // loop condition
				{

					if($key == 0){
						$where.=$codition['value'];
					}else{
						if($codition['type'] == 'and'){
							$where.= ' and '.$codition['value'];
						}else{
							$where.= ' or '.$codition['value'];
						}						
					}
				}	

				$where.=')';
			}

			$build['where']=$where;
		}

		if(!isset($this->build->set)){
			$build['field']= '';
			$build['insert_set']= '' ;
			$build['update_set']= '' ;
		}else{

			$update = '';

			$count = count($this->build->set);

			foreach ($this->build->set as $key=>$set) // loop condition
			{
				if($count == $key+1){
					$update.='`'.$set.'` = :'.$set;
				}else{
					$update.='`'.$set.'` = :'.$set.',';
				}
			}	
			
			$build['field'] = implode(',',$this->build->set);
			$build['insert_set']= ':'.implode(',:',$this->build->set);
			$build['update_set']= $update ;

		}


		$build['table'] = $table;
	
		switch($function){
			case 'select':
				$this->sql='SELECT :select FROM `:table` :join WHERE :where :group :order :limit';
			break;
			case 'update':
				$this->sql='UPDATE `:table` :join SET :update_set WHERE :where :limit';
			break;
			case 'insert':
				$this->sql='INSERT INTO `:table`(:field) VALUES(:insert_set)';
			break;
			case 'delete':
				$this->sql='DELETE FROM `:table` WHERE :where :limit';
			break;			
		}
	
		foreach ($build as $name=>$value){	
		  $check='/:'.$name.'/';
		  if (preg_match($check,$this->sql)) {
				$this->sql=str_replace(':'.$name,$value,$this->sql);
		  }	
		}	

		$this->build = new stdclass();

		return $this->sql;

	}

}

?>