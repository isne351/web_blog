<?php
class route
{
    private $route;
    private $instance;

    public function __construct($route_array)
    {
        $this->route = $route_array;
        $this->instance = get_instance();
    }

    private function find($route)
    {

        if(empty($route)){

            $route_component = explode('/',$this->route['index']);
            $file            = $route_component[0];
            $function        = $route_component[1];     
                   
            return array('file' => $file, 'function' => $function, 'params' => array());
            
        }else{

            foreach ($this->route as $key => $value) {

                $regex = '/^'.str_replace('/','\/',$key).'$/';

                if (preg_match($regex,$route,$match)){

                    $route_component = explode('/', $value);

                    $file     = $route_component[0];
                    $function = $route_component[1];

                    array_shift($route_component);
                    array_shift($route_component);

                    array_shift($match);

                    $params = array_merge($route_component,$match);                    

                    return array('file' => $file, 'function' => $function , 'params' => $params);
                }
            }

            $route_component = explode('/',$this->route['404']);
            $file            = $route_component[0];
            $function        = $route_component[1];
            return array('file' => $file, 'function' => $function , 'params' => array());

        }
    }

    public function exec($route)
    {
        $route = $this->find($route);

        $controller = $route['file'];
        $fucntion = $route['function'];

        $this->instance->loader->system->library('template');

        try {
            $this->instance->loader->app->control($controller);
            call_user_func_array(array($this->instance->$controller,$fucntion),$route['params']);
        } catch (Exception $e) {
            $this->instance->template->clear_view();

            if($this->instance->template->error_type == 'page'){
                $this->instance->template->set_var('exception',$e);
                $this->instance->template->set_view('body','error/error_exception');
            }else{
                result($e->getMessage());
            }
        }

        $this->instance->template->process();
        return $this->instance->template->show();

    }
}
