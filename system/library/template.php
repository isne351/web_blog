<?php
class template{
	
	private $template=array(
		'header'=>array('cache'=>false,'file'=>null),
		'navbar'=>array('cache'=>false,'file'=>null),
		'body'=>array('cache'=>false,'file'=>null),
		'footer'=>array('cache'=>false,'file'=>null)
	);

	public $variable = array();
	private $page = array();
    private $loader;

    private $type = 'html';

    public $error_type = 'page';

    public function __construct()
    {
        $this->loader = new loader(__BASE.'/views/');
    }

    public function set_type($type){
    	$this->type = $type;
    }

    public function set_var($key,$value){
    	$this->variable[$key] = $value;
    }

	public function set_view($type,$file,$cache = false){
		$this->template[$type] = array('file'=>$file,'cache'=>$cache);
	} 

	public function clear_view(){
		$this->template = array();
	}

	public function process(){
		
		ob_start();
		if($this->type == 'json'){
			header('Content-type: application/json');
			$this->page[] = json_encode($this->variable);
		}else{
				
			foreach ($this->template as $key=>$val){

				if($val['file'] == null){
					continue;
				}
				
				$this->loader->view($val['file'],$this->variable);	
				$this->page[$key]= ob_get_contents();	
				ob_clean();
			}
			
		}
		ob_end_clean();
	}
	
	public function show(){
		return implode('',$this->page);
	}
	
}
?>